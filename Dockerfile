FROM python:3.8-slim

WORKDIR /code

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN DEBIAN_FRONTEND=noninteractive apt-get update \
    && apt-get dist-upgrade -y \
    && apt-get install -y --no-install-recommends \
        procps netcat less telnet make \
    && rm -rf /var/lib/apt/lists/*

RUN pip3 install --upgrade pip
COPY requirements.txt .
RUN pip3 install -Ur requirements.txt

COPY . .

EXPOSE 8000
