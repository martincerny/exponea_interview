# Makefile
SHELL := /bin/bash -O globstar
.PHONY: help clean pep8 test

help:
	@echo "Please use 'make <target>' where <target> is one of"
	@echo "  clean                           clean up __pycache__"
	@echo "  pep8                            static check by flake8"
	@echo "  test                            run unittests"

clean:
	@echo "clean 'pycache'"
	rm -fv **/*.pyc

pep8:
	@echo "check pep8 (flake8)"
	flake8

test:
	@echo "unittests"
	pytest tests.py
