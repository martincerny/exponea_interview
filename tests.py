import json

import aiohttp
from aioresponses import aioresponses
import pytest

from httpx import AsyncClient

import server
from settings import TESTSERVER_URL


@pytest.fixture(scope="function")
def process_request_batch_3_success():
    return [
        (200, '{"time": 201}'),
        (200, '{"time": 161}'),
        (200, '{"time": 434}'),
    ]


@pytest.fixture(scope="function")
def process_request_batch_0_success():
    return []


@pytest.fixture(scope="function")
def response_status_data_1():
    return 200, {"time": 374}


@pytest.mark.asyncio
async def test_process_request(response_status_data_1):
    expected_status, expected_data = response_status_data_1
    with aioresponses() as mocked:
        mocked.get(
            TESTSERVER_URL,
            status=expected_status,
            body=json.dumps(expected_data),
        )
        session = aiohttp.ClientSession()
        status, text = await server.process_request(session)
        assert expected_status == status
        assert expected_data == text


@pytest.mark.asyncio
async def test_api_3_success(mocker, process_request_batch_3_success):
    mocker.patch.object(
        server,
        "process_request_batch",
        return_value=process_request_batch_3_success
    )
    async with AsyncClient(app=server.app, base_url="http://test") as ac:
        response = await ac.get("/api/all")
    assert response.status_code == 200
    assert response.json() == [t for s, t in process_request_batch_3_success]

    async with AsyncClient(app=server.app, base_url="http://test") as ac:
        response = await ac.get("/api/first")
    assert response.status_code == 200
    assert response.json() == process_request_batch_3_success[0][1]

    async with AsyncClient(app=server.app, base_url="http://test") as ac:
        response = await ac.get("/api/within-timeout")
    assert response.status_code == 200
    assert response.json() == [t for s, t in process_request_batch_3_success]


@pytest.mark.asyncio
async def test_api_no_success(mocker, process_request_batch_0_success):
    mocker.patch.object(
        server,
        "process_request_batch",
        return_value=process_request_batch_0_success
    )
    async with AsyncClient(app=server.app, base_url="http://test") as ac:
        response = await ac.get("/api/all")
    assert response.status_code == 504

    async with AsyncClient(app=server.app, base_url="http://test") as ac:
        response = await ac.get("/api/first")
    assert response.status_code == 504

    async with AsyncClient(app=server.app, base_url="http://test") as ac:
        response = await ac.get("/api/within-timeout")
    assert response.status_code == 200
    assert response.json() == []
