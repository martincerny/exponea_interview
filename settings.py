import os


TESTSERVER_URL = os.environ["TESTSERVER_URL"]
DEBUG = bool(os.environ.get("DEBUG", 0))
