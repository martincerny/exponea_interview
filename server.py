import asyncio
import time

import aiohttp
from fastapi import FastAPI, Response

from logger import logger
from settings import TESTSERVER_URL

app = FastAPI()  # pylint: disable=invalid-name


async def process_request(
        session: aiohttp.ClientSession, timeout: float = None):
    """
    The function calls GET request on TESTSERVER_URL and returns status_code
    and payload as text. Catch TimeoutError
    Args:
        session: opened session
        timeout: timeout in seconds
    Returns: (status_code, json_data)
    """
    kwargs = {}
    if timeout is not None:
        clienttimeout = aiohttp.ClientTimeout(total=timeout)
        kwargs = {"timeout": clienttimeout}
    logger.debug("sending request to %s with kwargs %s", session, kwargs)
    # time.perf_counter is better for time benchmarking
    # https://docs.python.org/3/library/time.html#time.perf_counter
    start = time.time()
    status_code, text = 0, ""
    try:
        async with session.get(TESTSERVER_URL, **kwargs) as response:
            status_code = response.status
            text = await response.json()
    except asyncio.TimeoutError:
        status_code = 504
        text = "Exponea server timeout error"
    except Exception:
        status_code = 500
        text = "Other unknown error"
    finally:
        delay = time.time() - start
        logger.debug(
            "received response (%s) after %s s: %s", status_code, delay, text)
    return status_code, text


async def process_request_batch(n: int, timeout: int = None):
    """
    Calls 'n' requests at the some time (asynchronously), waits for responses
    and returns them as list.
    Args:
        n: number of requests
        timeout: in miliseconds
    Returns: list of tuple (status_code, json_data)
        status_code(int): http status code
        json_data(dict): encoded payload
    """
    if timeout is not None:
        timeout = 0.001 * timeout
    async with aiohttp.ClientSession() as session:
        logger.debug("established connection session=%s", session)
        tasks = []
        for _ in range(n):
            task = asyncio.create_task(process_request(session, timeout))
            tasks.append(task)
        logger.debug("created %s tasks", len(tasks))
        # use asyncio.wait instead
        # https://docs.python.org/3.10/library/asyncio-task.html#asyncio.wait
        status_code_text_list = await asyncio.gather(*tasks)
        return status_code_text_list


def get_successful_responses(responses):
    return [
        data_dict
        for status_code, data_dict in responses
        if status_code == 200
    ]


@app.get("/api/all")
async def api_all(response: Response, timeout: int = None):
    # all successful responses
    logger.info("calling endpoint '/api/all' with timeout= %s ms", timeout)
    responses = await process_request_batch(3, timeout)
    successful_responses = get_successful_responses(responses)
    if not successful_responses:
        response.status_code = 504
    else:
        return successful_responses


@app.get("/api/first")
async def api_first(response: Response, timeout: int = None):
    # first successful response
    logger.info("calling endpoint '/api/first' with timeout= %s ms", timeout)
    responses = await process_request_batch(3, timeout)
    successful_responses = get_successful_responses(responses)
    if not successful_responses:
        response.status_code = 504
    else:
        return successful_responses[0]


@app.get("/api/within-timeout")
async def api_within_timeout(timeout: int = None):
    # all successful responses that return within a given timeout
    logger.info(
        "calling endpoint '/api/within-timeout' with timeout= %s ms", timeout)
    responses = await process_request_batch(3, timeout)
    return get_successful_responses(responses)


@app.get("/api/smart")
async def api_smart(response: Response):
    logger.info("calling endpoint '/api/smart'")
    responses = await process_request_batch(1, 300)
    successful_responses = get_successful_responses(responses)
    if successful_responses:
        return successful_responses[0]

    responses = await process_request_batch(2)
    successful_responses = get_successful_responses(responses)
    if not successful_responses:
        response.status_code = 504
    else:
        return successful_responses[0]
