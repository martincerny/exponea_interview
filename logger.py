import logging

from settings import DEBUG

logger = logging.getLogger(__name__)
logger.setLevel(logging.ERROR)
formatter = logging.Formatter("%(asctime)s | %(levelname)s | %(message)s")

fh = logging.FileHandler("error.log")
fh.setFormatter(formatter)
fh.setLevel(logging.ERROR)
logger.addHandler(fh)

fh = logging.FileHandler("access.log")
fh.setFormatter(formatter)
fh.setLevel(logging.INFO)
logger.addHandler(fh)

if DEBUG:
    logger.setLevel(logging.DEBUG)
    fh = logging.FileHandler("debug.log")
    fh.setFormatter(formatter)
    fh.setLevel(logging.DEBUG)
    logger.addHandler(fh)

    ch = logging.StreamHandler()
    ch.setFormatter(formatter)
    ch.setLevel(logging.ERROR)
    ch.setLevel(logging.DEBUG)
    logger.addHandler(ch)
