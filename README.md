# Exponea interview

## Task
* http server
* 3 different endpoints
* call 3 requests while calling my server
* focused on performance

I know django as http server. Django is very robust. The server should be focused on performance and concurrency. That's why I use asyncio and fastAPI.

## How to run it
* in debug mode `docker-compose up`
* in production mode `docker-compose -f docker-compose.prod.yml up`

## How to use it
You can call endpoints with or without `timeout` param. If `timeout` is not 
defined, Timeout is default (=300s, based on aiohttp)
```
http://localhost:8000/api/all?timeout=400

http://localhost:8000/api/all
```

## Unittests
There are 3 unittests. `test_process_request` tests function `process_request`
and mocks method `get` from ClientSession. Other 2 unittests test all required 
endpoints in both cases all and no successful responses.

#### TBD
* unittests for `process_request_batch`
* reached TimeoutError in `process_request`
* 4-th endpoint

## Links
* https://fastapi.tiangolo.com
* https://fastapi.tiangolo.com/advanced/async-tests
* https://docs.aiohttp.org/en/stable/testing.html
